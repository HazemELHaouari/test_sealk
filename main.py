import concurrent.futures
import sys
import threading
import click
import logging
from os import path, pardir
import pandas as pd

# Local imports
from common.dummy_ai import getCompanyAttractiveness

sys.path.append(path.abspath(path.join(path.dirname(__file__), pardir)))

# Logger definition
logging.basicConfig(format="%(message)s", level=logging.INFO)
thread_lock = threading.Lock()
ranking = []
top_companies = 10


def rank_attractiveness(attractiveness):
    global ranking
    global top_companies
    thread_lock.acquire()
    try:
        ranking = sort_attractiveness(attractiveness, top_companies, ranking)
    finally:
        thread_lock.release()
    return ranking


def sort_attractiveness(elmt, top, list_elmts=None, min_elmt=0, max_elmt=1):
    if list_elmts is None or len(list_elmts) == 0:
        return [elmt]
    if len(list_elmts) > 1:
        max_elmt = len(list_elmts)
    while min_elmt < max_elmt:
        mid = (max_elmt + min_elmt) // 2
        list_elmts[mid].get("score")
        if elmt.get("score") < list_elmts[mid].get("score"):
            max_elmt = mid
        else:
            min_elmt = mid + 1
    list_elmts.insert(min_elmt, elmt)
    return list_elmts[-top:]


def show_attractivenss(company_symbol):
    rank_attractiveness(getCompanyAttractiveness(company_symbol))


def get_companies_attractiveness(companies_symbol_list, threads=5):
    with concurrent.futures.ThreadPoolExecutor(max_workers=threads) as executor:
        executor.map(show_attractivenss, companies_symbol_list)


@click.command()
@click.option("--threads",
              default=5,
              help="number of threads")
@click.option(
    "--filename",
    default="./resources/nasdaq-company-list.csv",
    help="Input CSV file name"
)
@click.option(
    "--top",
    default=10,
    help="Number of companies Symbols to print at the end"
)
def main(filename: str, threads=1, top=10) -> None:
    global ranking
    global top_companies
    """
    Args:
        filename (str): Input CSV file name
        top (int, optional): Number of companies Symbols to print at the end. Defaults to 10.
    """
    top_companies = top
    company_names = pd.read_csv(filename)
    get_companies_attractiveness(company_names.Symbol.tolist(), threads)
    for company in ranking:
        logging.info(company)


if __name__ == "__main__":
    main()
