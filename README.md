# Company Ranking Run
To run this project to show the atractiveness of companies that you list:

```
python ./main.py --threads [thread_number] --top [companies_number] --filename [file_path]
```

- Threads: the threads number.
- Top: Number of companies Symbols to print at the end by default = 10
- Filename: Input CSV file nameby dealult is a file listed in ./resources